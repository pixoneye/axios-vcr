var md5 = require('md5')
var _ = require('lodash')

function key(axiosConfig) {
  //Content-Length is calculated automatically by Axios before sending a request
  //We don't want to include it here because it could be changed by axios

  const headersToSkip = [
    'Content-Length', 'content-length','common', 'delete', 'get',
    'head', 'patch', 'post' ,'put'
  ]

  let headers =  _.omit(Object.assign({}, axiosConfig.headers), headersToSkip)
  headers = {...headers, ...axiosConfig.headers.common, ...axiosConfig.headers[axiosConfig.method]}
  headers = _.sortBy(_.toPairs(headers), (pair) => pair[0])
  var baseConfig = {
    url: axiosConfig.url,
    method: axiosConfig.method,
    data: axiosConfig.data,
    headers
  }
  baseConfig = _.sortBy(_.toPairs(baseConfig), (pair) => pair[0])
  return md5(JSON.stringify(baseConfig))
}

module.exports = key
